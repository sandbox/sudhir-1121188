(function($) {
  $(document).ready(function() {
	  //show the Drupal.settings.colors value on load
	  showDrupalSettingsValue();
	  $('#edit-1119092-value').bind('click', function(){
		  	showDrupalSettingsValue();
		  	return false;
		  	});
  });
  
  var showDrupalSettingsValue = function() {
    $('#found_result').html(Drupal.settings.colors.toString());	  
  }
})(jQuery);